/*
 * File:   main.c
 * Author: rbeal
 *
 * Created on December 30, 2017, 9:30 PM
 */

#include <xc.h>
#include "types.h"
#include "ios.h"
#include "soft_timer.h"
#include "timer.h"
#include "sdcard/defines.h"

#include "sdcard/fileio.h"
#include "sdcard/drv_spi.h"
#include "sdcard/rtcc.h"
#include "sdcard/sd_spi.h"
#include "sdcard/init.h"

#include "usb/usb_device.h"
#include "usb/define_usb.h"
#include "keyboard.h"

void ioInit(void);

extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters;
DEMO_STATE demoState = DEMO_STATE_NO_MEDIA;
FILEIO_OBJECT file;

u8 sampleData[9] = "sdus !\r\n";
u8 cmd[3][50] = {
    {"route\n"},
    {"ip link\n"},
    {"ps -ef\n"},
};

const FILEIO_DRIVE_CONFIG gSdDrive =
{
    (FILEIO_DRIVER_IOInitialize)FILEIO_SD_IOInitialize,                      // Function to initialize the I/O pins used by the driver.
    (FILEIO_DRIVER_MediaDetect)FILEIO_SD_MediaDetect,                       // Function to detect that the media is inserted.
    (FILEIO_DRIVER_MediaInitialize)FILEIO_SD_MediaInitialize,               // Function to initialize the media.
    (FILEIO_DRIVER_MediaDeinitialize)FILEIO_SD_MediaDeinitialize,           // Function to de-initialize the media.
    (FILEIO_DRIVER_SectorRead)FILEIO_SD_SectorRead,                         // Function to read a sector from the media.
    (FILEIO_DRIVER_SectorWrite)FILEIO_SD_SectorWrite,                       // Function to write a sector to the media.
    (FILEIO_DRIVER_WriteProtectStateGet)FILEIO_SD_WriteProtectStateGet,     // Function to determine if the media is write-protected.
};

int main(void) {
    
    u8 sdcard_state = 0;
    u8 ouaip[3];
    u8 i;
    
    s16 read;
    u8 read_ctn_line;
    u8 read_ctn_char;
    
    
    ioInit();
    timer_1_init();
    soft_timer_init();
    
    ios_led_init();
    ios_button_init();
    
    init_sd_fileio();
   
    
    SYSTEM_Initialize( SYSTEM_STATE_USB_START );
    USBDeviceInit();
    USBDeviceAttach();
    cirbuf_keyboard_init();
    ascii_conv_init();
    
    for (;;) {
        
        ios_led_machine();
        ios_button_machine();
        APP_KeyboardTasks();
        
        
        if (ios_button_getState(BUTTON_1) == BUTTON_PRESSED) {
            if (ouaip[0] == 0) {
                for (i=0;i<strlen(cmd[0]);i++) {
                    keyboard_putc(cmd[0][i]);
                    ouaip[0] = 1;
                }
            }
        }
        else {
            ouaip[0] = 0;
        }
        
        if (ios_button_getState(BUTTON_2) == BUTTON_PRESSED) {
            if (ouaip[1] == 0) {
                for (i=0;i<strlen(cmd[1]);i++) {
                    keyboard_putc(cmd[1][i]);
                    ouaip[1] = 1;
                }
            }
        }
        else {
            ouaip[1] = 0;
        }
        
        if (ios_button_getState(BUTTON_3) == BUTTON_PRESSED) {
            if (ouaip[2] == 0) {
                for (i=0;i<strlen(cmd[2]);i++) {
                    keyboard_putc(cmd[2][i]);
                    ouaip[2] = 1;
                }
            }
        }
        else {
            ouaip[2] = 0;
        }
        
        if (ios_button_getState(BUTTON_4) == BUTTON_PRESSED)
            asm("reset"); // For freestyle reset button
        
        if (PORTAbits.RA9 == 0)
            ios_led_setState(LED_9, LED_ON);
        else
            ios_led_setState(LED_9, LED_OFF);
        
        
        switch (sdcard_state) {
            case 0:
                if (PORTAbits.RA9 == 0 && FILEIO_MediaDetect(&gSdDrive, &sdCardMediaParameters) == 1)
                    sdcard_state = 1;
                break;
                
            case 1:
                if ((FILEIO_DriveMount('A', &gSdDrive, &sdCardMediaParameters) == FILEIO_ERROR_NONE))
                    sdcard_state = 2;
                break;
                
            case 2:
                if ((FILEIO_Open(&file, "TRUC.TXT", FILEIO_OPEN_READ | FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND) != FILEIO_RESULT_FAILURE)) {
                    sdcard_state = 3;
                    read_ctn_char = 0;
                    read_ctn_line = 0;
                    FILEIO_Seek(&file, 0, FILEIO_SEEK_SET);
                }
                break;
                
            case 3:
                //FILEIO_Write (sampleData, 1, 8, &file);
                
                if ((read = FILEIO_GetChar(&file)) != FILEIO_RESULT_FAILURE) {
                    
                    if (read == 0x0D)
                        break;
                    
                    ios_led_setState(LED_8, LED_ON);
                    if (read == 0x0A) {
                        cmd[read_ctn_line][read_ctn_char++] = '\n';
                        cmd[read_ctn_line][read_ctn_char++] = 0;
                        read_ctn_char = 0;
                        read_ctn_line++;
                    }
                    else
                        cmd[read_ctn_line][read_ctn_char++] = read;
                    
                    if (read_ctn_char > 20 || read_ctn_line > 2)
                        sdcard_state = 4;
                }
                else
                    sdcard_state = 4;
                break;
                    
            case 4:
                if ((FILEIO_Close (&file) == FILEIO_RESULT_SUCCESS))
                    sdcard_state = 5;
                break;
                
            case 5:
                if ((FILEIO_DriveUnmount ('A') != FILEIO_RESULT_SUCCESS))
                    sdcard_state = 6;
                break;
                
            case 6:
                ios_led_setState(LED_1, LED_BLINK);
                break;
                
            default :
                // mange tes jambes
                break;
                
        }
        
        
        
        
    }
    
    return 1;
}


void ioInit(void) {
    
    // Disable alls Analog input
    ANSA = 0;
    ANSB = 0;
    ANSC = 0;
    
    TRISCbits.TRISC8 = 0; // CS spi : output mode
    TRISAbits.TRISA9 = 1; // sdcard Presence
    
    OSCCONbits.IOLOCK = 0; // unlock PPS
    RPOR7bits.RP14R = _RPOUT_SDO1;
    RPOR7bits.RP15R = _RPOUT_SCK1OUT;
    RPINR20bits.SDI1R = 25;
    OSCCONbits.IOLOCK = 1; // lock PPS
    
}

inline void USER_SdSpiSetCs(uint8_t a)
{
    LATCbits.LATC8 = a;
}

inline bool USER_SdSpiGetCd(void)
{
    TRISCbits.TRISC5 = 0;
    
    if (PORTAbits.RA9)
        return false;
    else
        return true;
}

inline bool USER_SdSpiGetWp(void)
{
    return false;
}

void USER_SdSpiConfigurePins(void)
{
    
    RPOR7bits.RP14R = _RPOUT_SDO1;
    RPOR7bits.RP15R = _RPOUT_SCK1OUT;
    RPINR20bits.SDI1R = 25;
    
    // Deassert the chip select pin
    LATCbits.LATC8 = 1;
    // Configure CS pin as an output
    TRISCbits.TRISC8 = 0;
    // Configure CD pin as an input
    TRISAbits.TRISA9 = 1;
    // Configure WP pin as an input
    //TRISFbits.TRISF1 = 1;
}

FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters =
{
    1,                                  // Use SPI module 2
    USER_SdSpiSetCs,                    // User-specified function to set/clear the Chip Select pin.
    USER_SdSpiGetCd,                    // User-specified function to get the status of the Card Detect pin.
    USER_SdSpiGetWp,                    // User-specified function to get the status of the Write Protect pin.
    USER_SdSpiConfigurePins             // User-specified function to configure the pins' TRIS bits.
};


void SYSTEM_Initialize(SYSTEM_STATE state) {
    switch(state)
    {
        case SYSTEM_STATE_USB_START:        
            //Make sure that the general purpose output driver multiplexed with
            //the VBUS pin is always consistently configured to be tri-stated in
            //USB applications, so as to avoid any possible contention with the host.
            //(ex: maintain TRISBbits.TRISB6 = 1 at all times).
            TRISBbits.TRISB6 = 1;
            
            
            //BUTTON_Enable(BUTTON_USB_DEVICE_HID_KEYBOARD_KEY);
            break;
            
        case SYSTEM_STATE_USB_SUSPEND:
            //If developing a bus powered USB device that needs to be USB compliant,
            //insert code here to reduce the I/O pin and microcontroller power consumption,
            //so that the total current is <2.5mA from the USB host's VBUS supply.
            //If developing a self powered application (or a bus powered device where
            //official USB compliance isn't critical), nothing strictly needs
            //to be done during USB suspend.

            USBSleepOnSuspend();
            break;

        case SYSTEM_STATE_USB_RESUME:
            //If you didn't change any I/O pin states prior to entry into suspend,
            //then nothing explicitly needs to be done here.  However, by the time
            //the firmware returns from this function, the full application should
            //be restored into effectively exactly the same state as the application
            //was in, prior to entering USB suspend.
            
            //Additionally, before returning from this function, make sure the microcontroller's
            //currently active clock settings are compatible with USB operation, including
            //taking into consideration all possible microcontroller features/effects
            //that can affect the oscillator settings (such as internal/external 
            //switchover (two speed start up), fail-safe clock monitor, PLL lock time,
            //external crystal/resonator startup time (if using a crystal/resonator),
            //etc.

            //Additionally, the official USB specifications dictate that USB devices
            //must become fully operational and ready for new host requests/normal 
            //USB communication after a 10ms "resume recovery interval" has elapsed.
            //In order to meet this timing requirement and avoid possible issues,
            //make sure that all necessary oscillator startup is complete and this
            //function has returned within less than this 10ms interval.

            break;

        default:
            break;
    }
}

void __attribute__((interrupt,auto_psv)) _USB1Interrupt()
{
    USBDeviceTasks();
}


void update_led(void) {
    
    if(USBIsDeviceSuspended() == 1)
    {
        return;
    }
    
    switch(USBGetDeviceState())
    {         
        case CONFIGURED_STATE:
            /* We are configured.  Blink fast.
             * On for 75ms, off for 75ms, then reset/repeat. */
            
            
            break;

        default:
            /* We aren't configured yet, but we aren't suspended so let's blink with
             * a slow pulse. On for 50ms, then off for 950ms, then reset/repeat. */
            
            break;
    }
}