/* 
 * File:   soft_timer.h
 * Author: rbeal
 *
 * Created on July 30, 2018, 1:42 PM
 */

#ifndef SOFT_TIMER_H
#define	SOFT_TIMER_H

#include "types.h"

#define SOFT_TIMER_MACHINE_PERIOD   (0.001) // in sec
#define LED_PERIOD                  (0.5)/SOFT_TIMER_MACHINE_PERIOD // in sec

typedef enum {
    eTIMER_LED = 0,
    eNB_TIMERS
} eSoftTimerList;

typedef enum {
    eTIMER_STOPPED = 0,
    eTIMER_RUNNING,
    eTIMER_DONE
} eSoftTimerStates;



void soft_timer_init(void);
void soft_timer_machine(void);
eSoftTimerStates soft_timer_getState(eSoftTimerList timer);
void soft_timer_start(eSoftTimerList timer);
void soft_timer_stop(eSoftTimerList timer);
void soft_timer_set_period(eSoftTimerList timer, u32 period);
void ios_change_output(u8 lat, u8 val);

#endif	/* SOFT_TIMER_H */

