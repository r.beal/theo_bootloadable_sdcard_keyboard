/* 
 * File:   define_usb.h
 * Author: rbeal
 *
 * Created on August 2, 2018, 12:11 PM
 */

#ifndef DEFINE_USB_H
#define	DEFINE_USB_H

typedef enum
{
    SYSTEM_STATE_USB_START,
    SYSTEM_STATE_USB_SUSPEND,
    SYSTEM_STATE_USB_RESUME
} SYSTEM_STATE;

void SYSTEM_Initialize(SYSTEM_STATE state);
void update_led(void);

#endif	/* DEFINE_USB_H */

