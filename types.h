/* 
 * File:   types.h
 * Author: rbeal
 *
 * Created on July 30, 2018, 1:45 PM
 */

#ifndef TYPES_H
#define	TYPES_H

typedef unsigned char u8;
typedef signed char s8;
typedef unsigned int u16;
typedef signed int s16;
typedef unsigned long int u32;
typedef signed long int s32;

#endif	/* TYPES_H */

