/* 
 * File:   defines.h
 * Author: rbeal
 *
 * Created on August 1, 2018, 12:23 AM
 */

#ifndef DEFINES_H
#define	DEFINES_H

#include "drv_spi.h"
#include <stdbool.h>
#include <stdint.h>
#include "fileio_config.h"
#include "sd_spi_config.h"

// Definition for system clock
#define SYS_CLK_FrequencySystemGet()        32000000
// Definition for peripheral clock
#define SYS_CLK_FrequencyPeripheralGet()    SYS_CLK_FrequencySystemGet()
// Definition for instruction clock
#define SYS_CLK_FrequencyInstructionGet()   (SYS_CLK_FrequencySystemGet() / 2)

#define MAIN_RETURN int

//Definitions letting SPI driver code know which SPI module to use
#define DRV_SPI_CONFIG_CHANNEL_1_ENABLE      // Enable SPI channel 1
//#define DRV_SPI_CONFIG_CHANNEL_2_ENABLE      // Enable SPI channel 2
//#define DRV_SPI_CONFIG_CHANNEL_3_ENABLE      // Enable SPI channel 3
//#define DRV_SPI_CONFIG_CHANNEL_4_ENABLE      // Enable SPI channel 4

#define USER_SPI_MODULE_NUM   1			//MSSP number used on this device



// User-defined function to set the chip select for our example drive
void USER_SdSpiSetCs(uint8_t a);
// User-defined function to get the card detection status for our example drive
bool USER_SdSpiGetCd(void);
// User-defined function to get the write-protect status for our example drive
bool USER_SdSpiGetWp(void);
// User-defined function to initialize tristate bits for CS, CD, and WP
void USER_SdSpiConfigurePins(void);

#endif	/* DEFINES_H */

